package com.jahanbabu.weatherli.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.jahanbabu.weatherli.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgetPasswordActivity extends AppCompatActivity {

	@BindView(R.id.emailEditText) EditText emailEditText;
	@BindView(R.id.signInTextView) TextView signInTextView;

    private FirebaseAuth mAuth;

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpass);
		ButterKnife.bind(this);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        mAuth = FirebaseAuth.getInstance();

        signInTextView.setPaintFlags(signInTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

	@OnClick(R.id.submitButton)
	public void onForgetPassword(View v) {
		Intent secretIntent = new Intent(ForgetPasswordActivity.this, SignInActivity.class);
		secretIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(secretIntent);
		overridePendingTransition(R.anim.right_in, R.anim.right_out);
		finish();
	}

	@OnClick(R.id.signInTextView)
	public void onSignInPressed(View v) {
		Intent signinIntent = new Intent(ForgetPasswordActivity.this, SignInActivity.class);
		signinIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(signinIntent);
		overridePendingTransition(R.anim.left_in, R.anim.left_out);
		finish();
	}
    
    @Override
    public void onBackPressed() {
    	Intent signinIntent = new Intent(ForgetPasswordActivity.this, SignInActivity.class);
		signinIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(signinIntent);
		overridePendingTransition(R.anim.left_in, R.anim.left_out);
		finish();
    	super.onBackPressed();
    }
}
