package com.jahanbabu.weatherli.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.jahanbabu.weatherli.R;
import com.jahanbabu.weatherli.utils.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.firstNameEditText)
    EditText firstNameEditText;
    @BindView(R.id.lastNameEditText)
    EditText lastNameEditText;
    @BindView(R.id.emailEditText)
    EditText emailEditText;
    @BindView(R.id.passwordEditText)
    EditText passwordEditText;
    @BindView(R.id.submitButton)
    Button submitButton;


    String TAG = SignupActivity.class.getSimpleName();

	private EditText mobileEditText;
	private TextView signinTextView;
    private String fName, lName, mobile, email, password;
    Context mContext;
    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);

        mContext = this;

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        mAuth = FirebaseAuth.getInstance();

//        mobileEditText = (EditText) findViewById(R.id.mobileEditText);
        signinTextView = findViewById(R.id.signInTextView);

        signinTextView.setPaintFlags(signinTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        signinTextView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent signinIntent = new Intent(SignupActivity.this, SignInActivity.class);
                signinIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(signinIntent);
				overridePendingTransition(R.anim.up_in, R.anim.up_out);
                finish();
			}
		});
    }

    @OnClick(R.id.submitButton)
    public void onCreateAccountClick(View arg0) {
        if(Utility.isNetworkAvailable(mContext)){
            fName = firstNameEditText.getText().toString();
            lName = lastNameEditText.getText().toString();
            email = emailEditText.getText().toString();
            password = passwordEditText.getText().toString();

            Timber.e("INPUT : " + fName+" ---- "+lName+" ---- "+password+" ---- "+password);

            if (!fName.isEmpty() && ! lName.isEmpty() && !email.isEmpty() && !password.isEmpty()){
                createAccount(fName, lName, email, password);
            } else
                Toast.makeText(SignupActivity.this, "Please fill all fields to create account", Toast.LENGTH_LONG).show();

        } else {
            Utility.showNoInternetDialog(mContext, SignupActivity.this);
        }
    }

    private void createAccount(String fName, String lName, String email, String password) {
        Log.d(TAG, "handleEmailPass:" + email + " - " + password);
        showProgressDialog();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");

                            updateUserProfile();

                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignupActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        hideProgressDialog();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Timber.e(e.toString());
            }
        });
    }

    private void updateUserProfile() {
        FirebaseUser user = mAuth.getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(fName + " " + lName)
                .build();

        showProgressDialog();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.e(TAG, "Update Profile Successful");
                            Toast.makeText(mContext, "Account Created Successfully", Toast.LENGTH_SHORT).show();
                            goToSignInView();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Update Failure");
                Toast.makeText(mContext, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void hideProgressDialog() {

    }

    private void showProgressDialog() {

    }

    @Override
    public void onBackPressed() {
    	Intent signinIntent = new Intent(SignupActivity.this, SignInActivity.class);
        signinIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(signinIntent);
		overridePendingTransition(R.anim.up_in, R.anim.up_out);
        finish();
    	super.onBackPressed();
    }

    public void goToSignInView() {
        Intent mainIntent = new Intent(SignupActivity.this, SignInActivity.class);
        startActivity(mainIntent);
        overridePendingTransition(R.anim.up_in, R.anim.up_out);
        finish();
    }

    public void requestSingup(final String body){
//        progressDialog = ProgressDialog.show(this, "","Creating account please wait...", true);
//        // Instantiate the RequestQueue.
//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url ="http://api.mysmartciti.com/iot/user/createUser";
//
//        // Request a string response from the provided URL.
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        progressDialog.dismiss();
//                        Timber.e("RESPONSE : " + response);
//                        try {
//                            JSONObject mainJsonObject = new JSONObject(response);
//                            if (response.contains("Invalid User")){
//                                Toast.makeText(SignupActivity.this, "Invalid User!!!", Toast.LENGTH_LONG).show();
//                            } else {
//                                status = mainJsonObject.optString("status");
//                                if (status.equalsIgnoreCase("1111")){
//                                    Toast.makeText(SignupActivity.this, "Account creation successful!!!", Toast.LENGTH_LONG).show();
//                                    Intent mainIntent = new Intent(SignupActivity.this, SignInActivity.class);
//                                    startActivity(mainIntent);
//                                    overridePendingTransition(R.anim.right_in, R.anim.right_out);
//                                    finish();
//                                } else if (status.equalsIgnoreCase("2222")){
//                                    Toast.makeText(SignupActivity.this, "This user is already exist!!!", Toast.LENGTH_LONG).show();
//                                } else
//                                    Toast.makeText(SignupActivity.this, "Error in account creation!!!", Toast.LENGTH_LONG).show();
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Timber.e("ERROR : " + error);
//                Toast.makeText(SignupActivity.this, "Server Error, Please try again!", Toast.LENGTH_LONG).show();
//            }
//        }){
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }
//
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                return body.getBytes();
//            }
//        };
//        // Add the request to the RequestQueue.
//        queue.add(stringRequest);
    }

}
