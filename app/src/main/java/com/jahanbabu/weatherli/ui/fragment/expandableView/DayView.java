package com.jahanbabu.weatherli.ui.fragment.expandableView;

import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.jahanbabu.weatherli.R;
import com.jahanbabu.weatherli.model.weeklyWeather.WeatherList;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.Collapse;
import com.mindorks.placeholderview.annotations.expand.Expand;
import com.mindorks.placeholderview.annotations.expand.Parent;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;
import com.mindorks.placeholderview.annotations.expand.SingleTop;
import com.mindorks.placeholderview.annotations.expand.Toggle;

import java.text.DecimalFormat;

@Parent
@SingleTop
@Layout(R.layout.row_day)
public class DayView {

//    @View(R.id.editImageView) private ImageView editImageView;
    @View(R.id.dateTextView) private TextView dateTextView;
    @View(R.id.descriptionTextView) private TextView descriptionTextView;
    @View(R.id.tempTextView) private TextView tempTextView;

//    @View(R.id.toggleIcon)
//    private ImageView toggleIcon;

    @Toggle(R.id.toggleView)
    private RelativeLayout toggleView;

    @ParentPosition
    private int mParentPosition;

    private Context mContext;
    private WeatherList weather;

    public DayView(Context context, WeatherList weather) {
        mContext = context;
        this.weather = weather;
    }

    @Resolve
    private void onResolved() {

        Log.e("onResolved ", " mParentPosition > " + mParentPosition);
        
//        toggleIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_expand_more));

        dateTextView.setText(weather.getDate());
        descriptionTextView.setText(weather.getWeather().get(0).getDescription());
        tempTextView.setText(new DecimalFormat("#.#").format(weather.getMain().getMinCelsius()) + "°C - "
                + new DecimalFormat("#.#").format(weather.getMain().getMaxCelsius()) + "°C");
    }

    @Expand
    private void onExpand(){
//        toggleIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_expand_less));
    }

    @Collapse
    private void onCollapse(){
//        toggleIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_expand_more));
    }
}