package com.jahanbabu.weatherli.ui.fragment.expandableView;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.jahanbabu.weatherli.R;
import com.jahanbabu.weatherli.model.weeklyWeather.WeatherList;
import com.jahanbabu.weatherli.utils.Utility;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import timber.log.Timber;

@Layout(R.layout.row_details)
public class DetailsView {

    private final java.util.List<WeatherList> weatherList;
    @ParentPosition
    private int mParentPosition;

    @ChildPosition
    private int mChildPosition;

    @View(R.id.chart) LineChart chart;

    private Context mContext;

    public DetailsView(Context context, List<WeatherList> weatherList) {
        mContext = context;
        this.weatherList = weatherList;
    }

    @Resolve
    private void onResolved() {

        Log.e("onResolved ", " mParentPosition > " + mParentPosition);

        List<Entry> entries = new ArrayList<>();
        List<String> values = new ArrayList<>();
        for (int i = 0; i < weatherList.size(); i++){
//            Timber.e("KELVIN : " + weatherList.get(i).getMain().getTemp());
//            Timber.e("DOUBLE : " + weatherList.get(i).getMain().getCelsius());
            Timber.e("FLOAT : " + (float) weatherList.get(i).getMain().getCelsius());
            Timber.e("TIME : " +  weatherList.get(i).getTime());

            entries.add(new Entry(i, (float) weatherList.get(i).getMain().getCelsius()));

            values.add(weatherList.get(i).getTime());
        }

        LineDataSet dataSet = new LineDataSet(entries, "Temperature"); // add entries to dataset

        dataSet.setValueTextSize(12f);
//        dataSet.setValueTextColor(Color.WHITE);
        dataSet.setCircleColor(Color.WHITE);
        dataSet.setLineWidth(2f);
        dataSet.setColor(Color.CYAN);
        dataSet.setCircleColorHole(Color.WHITE);

        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        chart.setDescription(null);
        chart.getLegend().setEnabled(false);
        chart.setDrawBorders(false);

        // remove axis
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setEnabled(false);
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
        chart.getXAxis().setDrawAxisLine(false);


        Timber.e("values : " +  values.size());
        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
//        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(12f);
        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));

        chart.invalidate(); // refresh
    }
}