package com.jahanbabu.weatherli.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jahanbabu.weatherli.R;
import com.jahanbabu.weatherli.model.weeklyWeather.WeatherList;
import com.jahanbabu.weatherli.model.weeklyWeather.WeeklyWeather;
import com.jahanbabu.weatherli.ui.HomeActivity;
import com.jahanbabu.weatherli.ui.fragment.expandableView.DayView;
import com.jahanbabu.weatherli.ui.fragment.expandableView.DetailsView;
import com.jahanbabu.weatherli.utils.ApiService;
import com.jahanbabu.weatherli.utils.Config;
import com.jahanbabu.weatherli.utils.Utility;
import com.mindorks.placeholderview.ExpandablePlaceHolderView;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class WeeklyFragment extends Fragment {
    @BindView(R.id.expandableView) ExpandablePlaceHolderView expandableView;
    @BindView(R.id.lastUpdateTextView) TextView lastUpdateTextView;
    @BindView(R.id.retryImageView) ImageView retryImageView;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    String TAG = WeeklyFragment.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    HomeActivity homeActivity;

    public WeeklyFragment() {
        // Required empty public constructor
    }

    public static WeeklyFragment newInstance(String param1, String param2) {
        WeeklyFragment fragment = new WeeklyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weekly, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        homeActivity = ((HomeActivity)getActivity());

        if (Config.lat > 0 && Config.lng > 0){
            if (Utility.isNetworkAvailable(getActivity()))
                updateWeather();
            else onUpdateFailed("No Internet connection!!!");
        } else {
            if (homeActivity.mRequestingLocationUpdates && homeActivity.checkPermissions()) {
                homeActivity.getCurrentLocation();
            } else if (!homeActivity.checkPermissions()) {
                homeActivity.requestPermissions(true, TAG);
            } else {
                homeActivity.requestGPSSetting(true, TAG);
            }
        }
    }

    @OnClick(R.id.retryImageView)
    public void onRetryClick(){
        if (Config.lat > 0 && Config.lng > 0){
            if (Utility.isNetworkAvailable(getActivity()))
                updateWeather();
            else {
                onUpdateFailed("No Internet connection!!!");
                Utility.showNoInternetDialog(getActivity(), getActivity());
            }
        } else {
            if (homeActivity.mRequestingLocationUpdates && homeActivity.checkPermissions()) {
                homeActivity.getCurrentLocation();
            } else if (!homeActivity.checkPermissions()) {
                homeActivity.requestPermissions(true, TAG);
            } else {
                homeActivity.requestGPSSetting(true, TAG);
            }
        }
    }

    public void updateWeather() {
        progressBar.setVisibility(View.VISIBLE);
        retryImageView.setVisibility(View.INVISIBLE);
        lastUpdateTextView.setText("Updating weather...");
        ApiService service = ApiService.retrofit.create(ApiService.class);

        final Call<WeeklyWeather> registerCall = service.requestWeeklyUpdate(Config.lat, Config.lng, Config.API_KEY);

        registerCall.enqueue(new Callback<WeeklyWeather>() {
            @Override
            public void onResponse(Call<WeeklyWeather> call, Response<WeeklyWeather> response) {
                progressBar.setVisibility(View.GONE);
                retryImageView.setVisibility(View.VISIBLE);
                Timber.e("RESPONSE" + response.raw().toString());
                if (response.raw().code() == 200){
                    if (response.body().getCod() != null && response.body().getCod().equalsIgnoreCase("200")){
                        Timber.e("RESPONSE Size : " + response.body().getWeatherList().size());
                        Date date = new Date();
                        String lastUpdate = Utility.updateDateFormat.format(date);
                        Hawk.put("lastWeeklyUpdate", "Last update : " + lastUpdate);
                        Hawk.put("lastWeeklyUpdateWeather", response.body());
                        updateUI();

                    }else {
                        onUpdateFailed("Something went wrong in server!!!");
                    }
                } else {
                    onUpdateFailed("Something went wrong in server!!!");
                }
            }

            @Override
            public void onFailure(Call<WeeklyWeather> call, Throwable t) {
                Timber.e("onFailure ---> " + t.toString());
                onUpdateFailed("Something went wrong in server!!!");
            }
        });
    }

    String cDate = "", pDate = "";
    List<WeatherList> tempList = new ArrayList<>();
    List<Double> temperatureList = new ArrayList<>();

    public void updateUI() {
        lastUpdateTextView.setText(Hawk.get("lastWeeklyUpdate", "Not updated"));
        retryImageView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        WeeklyWeather weather = Hawk.get("lastWeeklyUpdateWeather", null);
        if (weather == null)
            return;

        expandableView.removeAllViews();

        boolean isFirst = true;

        for (int i = 0; i < weather.getWeatherList().size(); i++){
            pDate = weather.getWeatherList().get(i).getDate();

            if (isFirst) {
                isFirst = false;
                cDate = pDate;
            }

            if (!cDate.equalsIgnoreCase(pDate)){
                updateList(pDate, weather.getWeatherList().get(i - tempList.size()), tempList, temperatureList);
            } else {
                tempList.add(weather.getWeatherList().get(i));
                temperatureList.add(weather.getWeatherList().get(i).getMain().getTemp());
            }
        }

        updateList(pDate, weather.getWeatherList().get(weather.getWeatherList().size() - tempList.size()), tempList, temperatureList);

        expandableView.expand(0);


    }

    private void updateList(String tempDate, WeatherList weatherList, List<WeatherList> tempWeatherList, List<Double> tempeList) {
        Double maxTemp = Collections.max(tempeList);
        Double minTemp = Collections.min(tempeList);
        weatherList.getMain().setTempMax(maxTemp);
        weatherList.getMain().setTempMin(minTemp);

        expandableView.addView(new DayView(getActivity(), weatherList));

        expandableView.addView(new DetailsView(getActivity(), tempWeatherList));

        cDate = tempDate;

        tempList = new ArrayList<>();
        temperatureList = new ArrayList<>();
    }

    public void onUpdateFailed(String msg) {
        progressBar.setVisibility(View.GONE);
        retryImageView.setVisibility(View.VISIBLE);
        Snackbar.make(getActivity().getWindow().getDecorView(), msg, Snackbar.LENGTH_LONG).show();

        updateUI();
    }
}
