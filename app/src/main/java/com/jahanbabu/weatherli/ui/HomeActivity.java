package com.jahanbabu.weatherli.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.jahanbabu.weatherli.BuildConfig;
import com.jahanbabu.weatherli.R;
import com.jahanbabu.weatherli.ui.fragment.TodayFragment;
import com.jahanbabu.weatherli.ui.fragment.WeeklyFragment;
import com.jahanbabu.weatherli.utils.Config;
import com.jahanbabu.weatherli.utils.Utility;
import com.orhanobut.hawk.Hawk;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.ObservableSource;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;

    private String TAG = HomeActivity.class.getSimpleName();
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 2*1000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 19;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    public Boolean mRequestingLocationUpdates;
    private Context mContext;
    private String uName, uEmail, uImage;
    private TextView userNameTextView, userEmailTextView;
    private ImageView profileImageView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Hawk.init(this).build();

        mContext = this;

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        profileImageView = headerView.findViewById(R.id.profileImageView);
        userNameTextView = headerView.findViewById(R.id.userNameTextView);
        userEmailTextView = headerView.findViewById(R.id.userEmailTextView);

        uName = Hawk.get("uName", "User");
        uEmail = Hawk.get("uEmail", "android@gmail.com");
        uImage = Hawk.get("uImage");
        userNameTextView.setText(uName);
        userEmailTextView.setText(uEmail);

        Glide.with(mContext)
                .load(uImage)
                .apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher))
                .into(profileImageView);

        mSettingsClient = LocationServices.getSettingsClient(this);

        mRequestingLocationUpdates = false;

        createLocationRequest();
        buildLocationSettingsRequest();

        goToFragment(TodayFragment.newInstance("",""), "TodayFragment");
    }

    public void goToFragment(Fragment fragment, String TAG) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped){ //fragment not in back stack, create it.
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.replace(R.id.fragmentContainer, fragment, TAG).addToBackStack(backStateName).commit();
        }
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setNumUpdates(1);
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
            if (getSupportFragmentManager().getBackStackEntryCount() == 1){
                finish();
            }
            else {
                super.onBackPressed();
            }
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_today) {
            goToFragment(TodayFragment.newInstance("",""), "TodayFragment");
        } else if (id == R.id.nav_weekly_update) {
            goToFragment(WeeklyFragment.newInstance("",""), "WeeklyFragment");
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                showLogoutDialog();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e(TAG, "User agreed to make required location settings changes.");
                        TodayFragment fragment = (TodayFragment) getSupportFragmentManager().findFragmentByTag("TodayFragment");
                        if (fragment != null && fragment.isVisible()) {
                            fragment.updateLocationIcon(true);
                        }
                        getCurrentLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e(TAG, "User chose not to make required location settings changes.");
                        Toast.makeText(mContext, "Turn on GPS to continue.", Toast.LENGTH_LONG).show();
                        updateUI();
                        break;
                }
                break;
        }
    }

    boolean isRequestedFromFragment = false;
    String fragmentTag = "";

    public void requestGPSSetting(boolean isRequested, String fragmentTag) {
        this.isRequestedFromFragment = isRequested;
        this.fragmentTag = fragmentTag;

        TodayFragment fragment = (TodayFragment) getSupportFragmentManager().findFragmentByTag("TodayFragment");

        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.e(TAG, "All location settings are satisfied.");

                        if (fragment != null && fragment.isVisible()) {
                            fragment.updateLocationIcon(true);
                        }

                        getCurrentLocation();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.e(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                if (fragment != null && fragment.isVisible()) {
                                    fragment.updateLocationIcon(false);
                                }
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(HomeActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    updateUI();
                                    Log.e(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                if (fragment != null && fragment.isVisible()) {
                                    fragment.updateLocationIcon(false);
                                }
                                Log.e(TAG, errorMessage);
                                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
                                updateUI();
                        }
                    }
                });
    }

    @SuppressLint("MissingPermission")
    public void getCurrentLocation(){
        if (!checkPermissions())
            return;

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(mContext);

        locationProvider
                .getLastKnownLocation()
                .switchIfEmpty(updateLocation(locationProvider))
                .subscribe(location -> {
                    if (location != null)
                        getWeatherWithLocation(location);
                    else Timber.e("location null");
                });
    }

    @SuppressLint("MissingPermission")
    private ObservableSource<? extends Location> updateLocation(ReactiveLocationProvider locationProvider) {
        return locationProvider.getUpdatedLocation(mLocationRequest);
    }

    private void getWeatherWithLocation(Location location) {
        Config.lat = location.getLatitude();
        Config.lng = location.getLongitude();
        Log.e("LastKnownLocation  :", Config.lat + "--" + Config.lng);

        if (isRequestedFromFragment && Utility.isNetworkAvailable(mContext)){
            isRequestedFromFragment = false;
            FragmentManager fm = getSupportFragmentManager();
            if (fragmentTag.equalsIgnoreCase("TodayFragment")){
                TodayFragment fragment = (TodayFragment)fm.findFragmentByTag(fragmentTag);
                fragment.updateWeather();
            } else if (fragmentTag.equalsIgnoreCase("WeeklyFragment")) {
                WeeklyFragment fragment = (WeeklyFragment)fm.findFragmentByTag(fragmentTag);
                fragment.updateWeather();
            }
        } else {
            Utility.showNoInternetDialog(mContext, HomeActivity.this);
            updateUI();
        }
    }

    public boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermissions(boolean isRequested, String TAG) {
        this.isRequestedFromFragment = isRequested;
        this.fragmentTag = fragmentTag;
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale) {
            Log.e(this.TAG, "Displaying permission rationale to provide additional mContext.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(HomeActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.e(this.TAG, "Requesting permission");
            ActivityCompat.requestPermissions(HomeActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void updateUI() {
        FragmentManager fm = getSupportFragmentManager();
        if (fragmentTag.equalsIgnoreCase("TodayFragment")){
            TodayFragment fragment = (TodayFragment)fm.findFragmentByTag(fragmentTag);
            fragment.updateUI();
        } else if (fragmentTag.equalsIgnoreCase("WeeklyFragment")) {
            WeeklyFragment fragment = (WeeklyFragment)fm.findFragmentByTag(fragmentTag);
            fragment.updateUI();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.e(TAG, "User interaction was cancelled.");
                updateUI();
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "Permission granted, updates requested");
                requestGPSSetting(true,fragmentTag);
            } else {
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    private void showLogoutDialog() {
        AlertDialog alertDialog;
        AlertDialog.Builder build = new AlertDialog.Builder(HomeActivity.this);
        build.setTitle("LOGOUT");
        build.setMessage("Do you really want to logout ?");
        build.setPositiveButton("YES", (dialog, which) -> {
            dialog.dismiss();
            FirebaseAuth.getInstance().signOut();
            finish();
            startActivity(new Intent(HomeActivity.this, SignInActivity.class));
        });

        build.setNegativeButton("NO", (dialog, which) -> {
            dialog.dismiss();
        });
        alertDialog = build.create();
        alertDialog.show();
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }
}
