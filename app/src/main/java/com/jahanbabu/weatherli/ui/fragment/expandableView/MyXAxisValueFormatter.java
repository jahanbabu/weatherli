package com.jahanbabu.weatherli.ui.fragment.expandableView;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.List;

public class MyXAxisValueFormatter implements IAxisValueFormatter {

    private List<String> mValues;

    public MyXAxisValueFormatter(List<String> values) {
        this.mValues = values;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        String val = null;
        try {
            val = mValues.get((int) value);
        } catch (IndexOutOfBoundsException e) {
            axis.setGranularityEnabled(false);
        }
        return val;
    }
}