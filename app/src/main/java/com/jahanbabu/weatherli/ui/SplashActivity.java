package com.jahanbabu.weatherli.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.jahanbabu.weatherli.BuildConfig;
import com.jahanbabu.weatherli.R;
import com.jahanbabu.weatherli.utils.FakeCrashLibrary;
import com.orhanobut.hawk.Hawk;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.appNameTextView) TextView appNameTextView;
	Context context;

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
		context = this;

		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

		Hawk.init(context).build();

		if (BuildConfig.DEBUG) {
			Timber.plant(new Timber.DebugTree());
		} else {
			Timber.plant(new CrashReportingTree());
		}

		RunAnimation();
    }

	private void RunAnimation()
	{
		Animation a = AnimationUtils.loadAnimation(this, R.anim.app_name_alpha);
		a.reset();
		appNameTextView.clearAnimation();
		appNameTextView.startAnimation(a);
		a.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				startActivity(new Intent(SplashActivity.this, SignInActivity.class));
				finish();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
	}

	/** A tree which logs important information for crash reporting. */
	private static class CrashReportingTree extends Timber.Tree {
		@Override
		protected void log(int priority, String tag, String message, Throwable t) {
			if (priority == Log.VERBOSE || priority == Log.DEBUG) {
				return;
			}

			FakeCrashLibrary.log(priority, tag, message);

			if (t != null) {
				if (priority == Log.ERROR) {
					FakeCrashLibrary.logError(t);
				} else if (priority == Log.WARN) {
					FakeCrashLibrary.logWarning(t);
				}
			}
		}
	}
}
