package com.jahanbabu.weatherli.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jahanbabu.weatherli.R;
import com.jahanbabu.weatherli.model.currentWeather.CurrentWeather;
import com.jahanbabu.weatherli.ui.HomeActivity;
import com.jahanbabu.weatherli.utils.ApiService;
import com.jahanbabu.weatherli.utils.Config;
import com.jahanbabu.weatherli.utils.Utility;
import com.orhanobut.hawk.Hawk;

import java.text.DecimalFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class TodayFragment extends Fragment {

    @BindView(R.id.locationIndicatorImageView) ImageView locationIndicatorImageView;
    @BindView(R.id.locationTextView) TextView locationTextView;
    @BindView(R.id.dateTextView) TextView dateTextView;
    @BindView(R.id.temperatureTextView) TextView temperatureTextView;
    @BindView(R.id.descriptionTextView) TextView descriptionTextView;
    @BindView(R.id.humidityTextView) TextView humidityTextView;
    @BindView(R.id.windTextView) TextView windTextView;
    @BindView(R.id.sunriseTextView) TextView sunriseTextView;
    @BindView(R.id.sunsetTextView) TextView sunsetTextView;
    @BindView(R.id.lastUpdateTextView) TextView lastUpdateTextView;
    @BindView(R.id.retryImageView) ImageView retryImageView;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    String TAG = TodayFragment.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    HomeActivity homeActivity;

    public TodayFragment() {
        // Required empty public constructor
    }

    public static TodayFragment newInstance(String param1, String param2) {
        TodayFragment fragment = new TodayFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_today, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        homeActivity = ((HomeActivity)getActivity());

        Date date = new Date();
        String cDate = Utility.currentDateFormat.format(date);

        dateTextView.setText(cDate);

        if (Config.lat > 0 && Config.lng > 0){
            if (Utility.isNetworkAvailable(getActivity()))
                updateWeather();
            else onUpdateFailed("No Internet connection!!!");
        } else {
            if (homeActivity.mRequestingLocationUpdates && homeActivity.checkPermissions()) {
                homeActivity.getCurrentLocation();
            } else if (!homeActivity.checkPermissions()) {
                homeActivity.requestPermissions(true, TAG);
            } else {
                homeActivity.requestGPSSetting(true, TAG);
            }
        }
    }

    @OnClick(R.id.retryImageView)
    public void onRetryClick(){
        if (Config.lat > 0 && Config.lng > 0){
            if (Utility.isNetworkAvailable(getActivity()))
                updateWeather();
            else {
                onUpdateFailed("No Internet connection!!!");
                Utility.showNoInternetDialog(getActivity(), getActivity());
            }
        } else {
            if (homeActivity.mRequestingLocationUpdates && homeActivity.checkPermissions()) {
                homeActivity.getCurrentLocation();
            } else if (!homeActivity.checkPermissions()) {
                homeActivity.requestPermissions(true, TAG);
            } else {
                homeActivity.requestGPSSetting(true, TAG);
            }
        }
    }

    public void updateLocationIcon(boolean isOn) {
        if (isOn){
            locationIndicatorImageView.setImageResource(R.drawable.ic_location_on);
        } else locationIndicatorImageView.setImageResource(R.drawable.ic_location_off);
    }

    public void updateWeather() {
        progressBar.setVisibility(View.VISIBLE);
        retryImageView.setVisibility(View.INVISIBLE);
        lastUpdateTextView.setText("Updating weather...");
        ApiService service = ApiService.retrofit.create(ApiService.class);

        final Call<CurrentWeather> registerCall = service.requestUpdate(Config.lat, Config.lng, Config.API_KEY);

        registerCall.enqueue(new Callback<CurrentWeather>() {
            @Override
            public void onResponse(Call<CurrentWeather> call, Response<CurrentWeather> response) {
                progressBar.setVisibility(View.GONE);
                retryImageView.setVisibility(View.VISIBLE);
                Timber.e("RESPONSE" + response.raw().toString());
                if (response.raw().code() == 200){
                    if (response.body().getCod() != null && response.body().getCod() == 200){
                        Date date = new Date();
                        String lastUpdate = Utility.updateDateFormat.format(date);
                        Hawk.put("lastUpdate", "Last update : " + lastUpdate);
                        Hawk.put("lastUpdateWeather", response.body());
                        updateUI();

                    }else {
                        onUpdateFailed("Something went wrong in server!!!");
                    }
                } else {
                    onUpdateFailed("Something went wrong in server!!!");
                }
            }

            @Override
            public void onFailure(Call<CurrentWeather> call, Throwable t) {
                Timber.e("onFailure ---> " + t.toString());
                onUpdateFailed("Something went wrong in server!!!");
            }
        });
    }

    public void updateUI() {
        lastUpdateTextView.setText(Hawk.get("lastUpdate", "Not updated"));
        progressBar.setVisibility(View.GONE);
        retryImageView.setVisibility(View.VISIBLE);

        CurrentWeather currentWeather = Hawk.get("lastUpdateWeather", null);
        if (currentWeather == null)
            return;

        temperatureTextView.setText(new DecimalFormat("#.#").format(currentWeather.getMain().getCelsius()) + "°C");
        locationTextView.setText(currentWeather.getName());
        descriptionTextView.setText(currentWeather.getWeather().get(0).getDescription());
        humidityTextView.setText(currentWeather.getMain().getHumidity() + "%");
        windTextView.setText(currentWeather.getWind().getSpeed() + "km/h  " + "  °" +currentWeather.getWind().getDeg());
        sunriseTextView.setText(currentWeather.getSys().getSunriseTime());
        sunsetTextView.setText(currentWeather.getSys().getSunsetTime());
    }

    private void onUpdateFailed(String msg) {
        progressBar.setVisibility(View.GONE);
        retryImageView.setVisibility(View.VISIBLE);
        Snackbar.make(getActivity().getWindow().getDecorView(), msg, Snackbar.LENGTH_LONG).show();

        updateUI();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
