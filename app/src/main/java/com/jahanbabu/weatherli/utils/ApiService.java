package com.jahanbabu.weatherli.utils;

import com.jahanbabu.weatherli.model.currentWeather.CurrentWeather;
import com.jahanbabu.weatherli.model.weeklyWeather.WeeklyWeather;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jahangirkabir on 14/03/18.
 */

public interface ApiService {
    @GET("weather")
    Call<CurrentWeather> requestUpdate(@Query("lat") double lat, @Query("lon") double lon, @Query("appid") String appid);

    @GET("forecast")
    Call<WeeklyWeather> requestWeeklyUpdate(@Query("lat") double lat, @Query("lon") double lon, @Query("appid") String appid);

    Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Config.BASE_URL)
            .client(Config.getClient())
            .build();
}