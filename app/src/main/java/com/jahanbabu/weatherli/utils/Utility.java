package com.jahanbabu.weatherli.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import timber.log.Timber;

public class Utility {
    public static String TAG = Utility.class.getSimpleName();

	public static SimpleDateFormat currentDateFormat = new SimpleDateFormat("EEE, MMM d");
	public static SimpleDateFormat updateDateFormat = new SimpleDateFormat("d MMM yyyy hh:mm a");
	public static SimpleDateFormat hourlyTimeFormat = new SimpleDateFormat("HH:mm");

	public static boolean isNetworkAvailable(Context context){
		ConnectivityManager cm =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
				activeNetwork.isConnectedOrConnecting();

		return isConnected;
	}

	public static void showAlertDialog(final Context context, String title, String msg) {
		AlertDialog dailog;
		Builder build = new Builder(context);
		build.setTitle(title);
		build.setMessage(msg);
		build.setPositiveButton("OK", (dialog, which) -> dialog.dismiss());
		dailog = build.create();
		dailog.show();
	}

	public static void showNoInternetDialog(final Context context, final Activity activity) {
		AlertDialog dailog;
		Builder build = new Builder(context);
		build.setMessage("This application requires Internet connection.\nWould you connect to internet ?");
		build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
			}
		});
		build.setNegativeButton("No", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				AlertDialog dailog;
//				ContextThemeWrapper ctw = new ContextThemeWrapper(mContext, R.style.MyAlertDialogStyle);
				Builder build = new Builder(context);
				build.setMessage("Are sure you want to exit?");
				build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						activity.finish();
					}
				});
				build.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dailog = build.create();
				dailog.show();
			}
		});
		dailog = build.create();
		dailog.show();
	}

	public static void showLongToast(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	public static void showShortToast(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	public static void showToast(Context context, String msg, int duration) {
		Toast.makeText(context, msg, duration).show();
	}

	public void showMessage(Context context, String title, String message) {
		Builder builder = new Builder(context);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}
	
	public Boolean isConnected(Context context) {
		Boolean status = false;

		ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInf = conMgr.getAllNetworkInfo();

		for (NetworkInfo inf : netInf) {
			if (inf != null) {
				if (inf.getTypeName().contains("WIFI")
						|| inf.getTypeName().toUpperCase().contains("MOBILE")) {
					if (inf.getState() == NetworkInfo.State.CONNECTED) {
						status = true;
						break;
					}
				}
			}
		}

		return status;

	}

	/** A tree which logs important information for crash reporting. */
	public static class CrashReportingTree extends Timber.Tree {
		@Override
        protected void log(int priority, String tag, String message, Throwable t) {
			if (priority == Log.VERBOSE || priority == Log.DEBUG) {
				return;
			}

			FakeCrashLibrary.log(priority, tag, message);

			if (t != null) {
				if (priority == Log.ERROR) {
					FakeCrashLibrary.logError(t);
				} else if (priority == Log.WARN) {
					FakeCrashLibrary.logWarning(t);
				}
			}
		}
	}
}
