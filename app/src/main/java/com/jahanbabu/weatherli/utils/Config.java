package com.jahanbabu.weatherli.utils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class Config {
	public static boolean isAdded = false;
	public static double lat = 0;
	public static double lng = 0;
	public static String API_KEY = "b27cbde0d429187a2a8d06cc7f89ff73";
	public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

	public static OkHttpClient getClient() {
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient client = new OkHttpClient.Builder()
				.connectTimeout(3, TimeUnit.MINUTES)
				.readTimeout(3, TimeUnit.MINUTES)
				.addInterceptor(logging)
				.build();
		return client;
	}
}
