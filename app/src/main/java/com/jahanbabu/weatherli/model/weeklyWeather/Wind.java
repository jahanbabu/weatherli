
package com.jahanbabu.weatherli.model.weeklyWeather;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Wind {

    @SerializedName("deg")
    private Double Deg;
    @SerializedName("speed")
    private Double Speed;

    public Double getDeg() {
        return Deg;
    }

    public void setDeg(Double deg) {
        Deg = deg;
    }

    public Double getSpeed() {
        return Speed;
    }

    public void setSpeed(Double speed) {
        Speed = speed;
    }

}
