package com.jahanbabu.weatherli.model.currentWeather;

import com.google.gson.annotations.SerializedName;

public class Coord {

    @SerializedName("lat")
    private Double Lat;
    @SerializedName("lon")
    private Double Lon;

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double lat) {
        Lat = lat;
    }

    public Double getLon() {
        return Lon;
    }

    public void setLon(Double lon) {
        Lon = lon;
    }

}
