package com.jahanbabu.weatherli.model.currentWeather;

import com.google.gson.annotations.SerializedName;

public class Clouds {

    @SerializedName("all")
    private Long All;

    public Long getAll() {
        return All;
    }

    public void setAll(Long all) {
        All = all;
    }

}
