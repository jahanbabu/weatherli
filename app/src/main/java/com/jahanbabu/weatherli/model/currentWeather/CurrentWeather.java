package com.jahanbabu.weatherli.model.currentWeather;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CurrentWeather {

    @SerializedName("base")
    private String Base;
    @SerializedName("clouds")
    private com.jahanbabu.weatherli.model.currentWeather.Clouds Clouds;
    @SerializedName("cod")
    private Long Cod;
    @SerializedName("coord")
    private com.jahanbabu.weatherli.model.currentWeather.Coord Coord;
    @SerializedName("dt")
    private Long Dt;
    @SerializedName("id")
    private Long Id;
    @SerializedName("main")
    private com.jahanbabu.weatherli.model.currentWeather.Main Main;
    @SerializedName("name")
    private String Name;
    @SerializedName("sys")
    private com.jahanbabu.weatherli.model.currentWeather.Sys Sys;
    @SerializedName("weather")
    private List<com.jahanbabu.weatherli.model.currentWeather.Weather> Weather;
    @SerializedName("wind")
    private com.jahanbabu.weatherli.model.currentWeather.Wind Wind;

    public String getBase() {
        return Base;
    }

    public void setBase(String base) {
        Base = base;
    }

    public com.jahanbabu.weatherli.model.currentWeather.Clouds getClouds() {
        return Clouds;
    }

    public void setClouds(com.jahanbabu.weatherli.model.currentWeather.Clouds clouds) {
        Clouds = clouds;
    }

    public Long getCod() {
        return Cod;
    }

    public void setCod(Long cod) {
        Cod = cod;
    }

    public com.jahanbabu.weatherli.model.currentWeather.Coord getCoord() {
        return Coord;
    }

    public void setCoord(com.jahanbabu.weatherli.model.currentWeather.Coord coord) {
        Coord = coord;
    }

    public Long getDt() {
        return Dt;
    }

    public void setDt(Long dt) {
        Dt = dt;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public com.jahanbabu.weatherli.model.currentWeather.Main getMain() {
        return Main;
    }

    public void setMain(com.jahanbabu.weatherli.model.currentWeather.Main main) {
        Main = main;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public com.jahanbabu.weatherli.model.currentWeather.Sys getSys() {
        return Sys;
    }

    public void setSys(com.jahanbabu.weatherli.model.currentWeather.Sys sys) {
        Sys = sys;
    }

    public List<com.jahanbabu.weatherli.model.currentWeather.Weather> getWeather() {
        return Weather;
    }

    public void setWeather(List<com.jahanbabu.weatherli.model.currentWeather.Weather> weather) {
        Weather = weather;
    }

    public com.jahanbabu.weatherli.model.currentWeather.Wind getWind() {
        return Wind;
    }

    public void setWind(com.jahanbabu.weatherli.model.currentWeather.Wind wind) {
        Wind = wind;
    }

}
