package com.jahanbabu.weatherli.model.currentWeather;

import com.google.gson.annotations.SerializedName;

public class Main {

    @SerializedName("grnd_level")
    private Double GrndLevel;
    @SerializedName("humidity")
    private Long Humidity;
    @SerializedName("pressure")
    private Double Pressure;
    @SerializedName("sea_level")
    private Double SeaLevel;
    @SerializedName("temp")
    private Double Temp;
    @SerializedName("temp_max")
    private Double TempMax;
    @SerializedName("temp_min")
    private Double TempMin;

    public Double getGrndLevel() {
        return GrndLevel;
    }

    public void setGrndLevel(Double grndLevel) {
        GrndLevel = grndLevel;
    }

    public Long getHumidity() {
        return Humidity;
    }

    public void setHumidity(Long humidity) {
        Humidity = humidity;
    }

    public Double getPressure() {
        return Pressure;
    }

    public void setPressure(Double pressure) {
        Pressure = pressure;
    }

    public Double getSeaLevel() {
        return SeaLevel;
    }

    public void setSeaLevel(Double seaLevel) {
        SeaLevel = seaLevel;
    }

    public Double getTemp() {
        return Temp;
    }

    public void setTemp(Double temp) {
        Temp = temp;
    }

    public Double getTempMax() {
        return TempMax;
    }

    public void setTempMax(Double tempMax) {
        TempMax = tempMax;
    }

    public Double getTempMin() {
        return TempMin;
    }

    public void setTempMin(Double tempMin) {
        TempMin = tempMin;
    }

    public double getCelsius() {
        double c = Temp - 273.16;
        return c;
    }

    public double getFahrenheit() {
        double f = (((Temp - 273) * 9/5) + 32);
        return f;
    }
}
