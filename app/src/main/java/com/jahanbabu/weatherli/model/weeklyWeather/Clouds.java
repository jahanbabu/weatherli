
package com.jahanbabu.weatherli.model.weeklyWeather;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Clouds {

    @SerializedName("all")
    private Long All;

    public Long getAll() {
        return All;
    }

    public void setAll(Long all) {
        All = all;
    }

}
