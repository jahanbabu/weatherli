package com.jahanbabu.weatherli.model.currentWeather;

import com.google.gson.annotations.SerializedName;
import com.jahanbabu.weatherli.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Sys {

    @SerializedName("country")
    private String Country;
    @SerializedName("message")
    private Double Message;
    @SerializedName("sunrise")
    private Long Sunrise;
    @SerializedName("sunset")
    private Long Sunset;

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public Double getMessage() {
        return Message;
    }

    public void setMessage(Double message) {
        Message = message;
    }

    public Long getSunrise() {
        return Sunrise;
    }

    public String getSunriseTime() {
        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getTimeZone("UTC");
            calendar.setTimeInMillis(Sunrise * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "N/A";
    }

    public void setSunrise(Long sunrise) {
        Sunrise = sunrise;
    }

    public Long getSunset() {
        return Sunset;
    }

    public String getSunsetTime() {
        try{
            Calendar calendar = Calendar.getInstance();
//            TimeZone tz = TimeZone.getDefault();
            TimeZone tz = TimeZone.getTimeZone("UTC");
            calendar.setTimeInMillis(Sunset * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "N/A";
    }

    public void setSunset(Long sunset) {
        Sunset = sunset;
    }

}
