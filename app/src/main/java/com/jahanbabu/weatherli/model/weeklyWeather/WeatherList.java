package com.jahanbabu.weatherli.model.weeklyWeather;

import com.google.gson.annotations.SerializedName;
import com.jahanbabu.weatherli.utils.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherList {

    @SerializedName("clouds")
    private com.jahanbabu.weatherli.model.weeklyWeather.Clouds Clouds;
    @SerializedName("dt")
    private Long Dt;
    @SerializedName("dt_txt")
    private String DtTxt;
    @SerializedName("main")
    private com.jahanbabu.weatherli.model.weeklyWeather.Main Main;
    @SerializedName("rain")
    private com.jahanbabu.weatherli.model.weeklyWeather.Rain Rain;
    @SerializedName("sys")
    private com.jahanbabu.weatherli.model.weeklyWeather.Sys Sys;
    @SerializedName("weather")
    private java.util.List<com.jahanbabu.weatherli.model.weeklyWeather.Weather> Weather;
    @SerializedName("wind")
    private com.jahanbabu.weatherli.model.weeklyWeather.Wind Wind;

    public com.jahanbabu.weatherli.model.weeklyWeather.Clouds getClouds() {
        return Clouds;
    }

    public void setClouds(com.jahanbabu.weatherli.model.weeklyWeather.Clouds clouds) {
        Clouds = clouds;
    }

    public Long getDt() {
        return Dt;
    }

    public void setDt(Long dt) {
        Dt = dt;
    }

    public String getDtTxt() {
        return DtTxt;
    }

    public void setDtTxt(String dtTxt) {
        DtTxt = dtTxt;
    }

    public String getDate() {
        String date = "";
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date newDate = spf.parse(DtTxt);
            date = Utility.currentDateFormat.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public String getTime() {
        String time = "";
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date newDate = spf.parse(DtTxt);
            time = Utility.hourlyTimeFormat.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    public com.jahanbabu.weatherli.model.weeklyWeather.Main getMain() {
        return Main;
    }

    public void setMain(com.jahanbabu.weatherli.model.weeklyWeather.Main main) {
        Main = main;
    }

    public com.jahanbabu.weatherli.model.weeklyWeather.Rain getRain() {
        return Rain;
    }

    public void setRain(com.jahanbabu.weatherli.model.weeklyWeather.Rain rain) {
        Rain = rain;
    }

    public com.jahanbabu.weatherli.model.weeklyWeather.Sys getSys() {
        return Sys;
    }

    public void setSys(com.jahanbabu.weatherli.model.weeklyWeather.Sys sys) {
        Sys = sys;
    }

    public java.util.List<com.jahanbabu.weatherli.model.weeklyWeather.Weather> getWeather() {
        return Weather;
    }

    public void setWeather(java.util.List<com.jahanbabu.weatherli.model.weeklyWeather.Weather> weather) {
        Weather = weather;
    }

    public com.jahanbabu.weatherli.model.weeklyWeather.Wind getWind() {
        return Wind;
    }

    public void setWind(com.jahanbabu.weatherli.model.weeklyWeather.Wind wind) {
        Wind = wind;
    }

}
