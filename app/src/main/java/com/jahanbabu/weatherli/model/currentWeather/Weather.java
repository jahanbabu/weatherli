package com.jahanbabu.weatherli.model.currentWeather;

import com.google.gson.annotations.SerializedName;

public class Weather {

    @SerializedName("description")
    private String Description;
    @SerializedName("icon")
    private String Icon;
    @SerializedName("id")
    private Long Id;
    @SerializedName("main")
    private String Main;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getMain() {
        return Main;
    }

    public void setMain(String main) {
        Main = main;
    }

}
