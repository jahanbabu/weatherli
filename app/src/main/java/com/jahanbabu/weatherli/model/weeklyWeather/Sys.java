
package com.jahanbabu.weatherli.model.weeklyWeather;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Sys {

    @SerializedName("pod")
    private String Pod;

    public String getPod() {
        return Pod;
    }

    public void setPod(String pod) {
        Pod = pod;
    }

}
