
package com.jahanbabu.weatherli.model.weeklyWeather;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class City {

    @SerializedName("coord")
    private com.jahanbabu.weatherli.model.weeklyWeather.Coord Coord;
    @SerializedName("country")
    private String Country;
    @SerializedName("id")
    private Long Id;
    @SerializedName("name")
    private String Name;

    public com.jahanbabu.weatherli.model.weeklyWeather.Coord getCoord() {
        return Coord;
    }

    public void setCoord(com.jahanbabu.weatherli.model.weeklyWeather.Coord coord) {
        Coord = coord;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

}
