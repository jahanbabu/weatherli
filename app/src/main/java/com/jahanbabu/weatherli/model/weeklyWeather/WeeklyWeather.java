package com.jahanbabu.weatherli.model.weeklyWeather;

import com.google.gson.annotations.SerializedName;

public class WeeklyWeather {

    @SerializedName("city")
    private com.jahanbabu.weatherli.model.weeklyWeather.City City;
    @SerializedName("cnt")
    private Long Cnt;
    @SerializedName("cod")
    private String Cod;
    @SerializedName("list")
    private java.util.List<WeatherList> WeatherList;
    @SerializedName("message")
    private Double Message;

    public com.jahanbabu.weatherli.model.weeklyWeather.City getCity() {
        return City;
    }

    public void setCity(com.jahanbabu.weatherli.model.weeklyWeather.City city) {
        City = city;
    }

    public Long getCnt() {
        return Cnt;
    }

    public void setCnt(Long cnt) {
        Cnt = cnt;
    }

    public String getCod() {
        return Cod;
    }

    public void setCod(String cod) {
        Cod = cod;
    }

    public java.util.List<WeatherList> getWeatherList() {
        return WeatherList;
    }

    public void setWeatherList(java.util.List<WeatherList> weatherList) {
        WeatherList = weatherList;
    }

    public Double getMessage() {
        return Message;
    }

    public void setMessage(Double message) {
        Message = message;
    }

}
